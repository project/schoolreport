<?php

/**
 * @file
 * Views exposure code for custom tables created by School Report module.
 */

/**
 *  This file is used to tell the views module about the schoolreport_nodes
 * and schoolreport_allocations tables.
 *
 * Database definition:
 * @code
 *   CREATE TABLE `schoolreport_nodes` (
 *   `nid` int(10) unsigned NOT NULL DEFAULT '0',
 *   `student_uid` int(10) unsigned DEFAULT '0',
 *   `fee` decimal(10,2) NOT NULL DEFAULT '0.00',
 *   `total_fee` decimal(10,2) NOT NULL DEFAULT '0.00',
 *   PRIMARY KEY (`nid`)
 *   )
 *
 *   CREATE TABLE `schoolreport_allocations` (
 *   `student_uid` int(10) unsigned NOT NULL DEFAULT '0',
 *   `teacher_uid` int(11) NOT NULL DEFAULT '0',
 *   `active` int(10) unsigned NOT NULL DEFAULT '1',
 *   PRIMARY KEY (`student_uid`)
 *   )
 * @endcode
 */

/**
 * Implementation of hook_views_data().
 */
function schoolreport_views_data() {

  $data['schoolreport_nodes']['table'] = array(
    'group' => t('School Report allocation'),
    'title' => t('School Report allocation'),
    'help' => t('Provides functionality for allocating students to teachers'),
  );

  $data['schoolreport_nodes']['table']['join'] = array(
    'node' => array(
      'left_field' => 'nid',
      'field' => 'nid',
      ),
  );

  $data['schoolreport_nodes']['nid'] = array(
    'title' => t('Node'),
    'help' => t('The nid of the node.'),
    'field' => array(
      'handler' => 'views_handler_field',
    ),
  );

  /* $data['schoolreport_nodes']['student_uid'] = array(
    'title' => t('Student\'s uid'),
    'help' => t('The uid of the student referred to in the node.'),
    'field' => array(
      'handler' => 'views_handler_field',
    ),
  );*/

  $data['schoolreport_nodes']['student_uid'] = array(
    'title' => t('User'),
    'help' => t('The selected student.'),
    'relationship' => array(
      'base' => 'users',
      'handler' => 'views_handler_relationship',
      'label' => t('Student'),
    ),
  );

  $data['schoolreport_allocations']['table'] = array(
    'group' => t('School Report allocation'),
    'title' => t('School Report allocation'),
    'help' => t('Provides functionality for allocating learners to tutors'),
  );

  $data['schoolreport_allocations']['table']['join'] = array(
    'users' => array(
      'left_field' => 'uid',
      'field' => 'student_uid',
    ),
  );

  $data['schoolreport_allocations']['table']['join'] = array(
    'node' => array(
      'left_table' => 'schoolreport_nodes',
      'left_field' => 'student_uid',
      'field' => 'student_uid',
    ),
  );


  $data['schoolreport_allocations']['student_uid'] = array(
    'title' => t('Student\'s uid'),
    'help' => t('The uid of the student, taken from the allocations table'),
    'field' => array(
      'handler' => 'views_handler_field',
    ),
  );

  $data['schoolreport_allocations']['teacher_uid'] = array(
    'title' => t('Teacher uid'),
    'help' => t('The uid of the teacher'),
    'field' => array(
      'handler' => 'views_handler_field',
    ),
  );

  return $data;
}