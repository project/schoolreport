<?php

/**
 * @file
 * Administration page callbacks for the School Report module.
 */

function schoolreport_admin_settings() {
  $node_types = node_get_types('names');

  $form['schoolreport_node_types'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Reports'),
    '#options' => $node_types,
    '#default_value' => variable_get('schoolreport_node_types', array('')),
    '#description' => t('A select box of students will be available on these content types.'),
  );

  $user_roles = user_roles($membersonly = TRUE, $permission = NULL);

  $form['schoolreport_teachers'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Teachers'),
    '#options' => $user_roles,
    '#default_value' => variable_get('schoolreport_teachers', array('')),
    '#description' => t('Users with these roles can have students allocated to them.'),
  );

  $form['schoolreport_students'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Students'),
    '#options' => $user_roles,
    '#default_value' => variable_get('schoolreport_students', array('')),
    '#description' => t('Users with these roles can be allocated to teachers.'),
  );

  $form['schoolreport_administrators'] = array(
    '#type' => 'checkboxes',
    '#title' => t('School report administrators'),
    '#options' => $user_roles,
    '#default_value' => variable_get('schoolreport_administrators', array('')),
    '#description' => t('Users with these roles can change all of these settings.'),
  );

  return system_settings_form($form);

}